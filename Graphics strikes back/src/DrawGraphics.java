import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class DrawGraphics {
	ArrayList<Mover> drawable = new ArrayList<Mover>();
	//ArrayList<Bouncer> bouncers = new ArrayList<Bouncer>();
	//ArrayList<StraightMover> movers = new ArrayList<StraightMover>();
    //Bouncer movingSprite;

    /** Initializes this class for drawing. */
    public DrawGraphics() {
        Rectangle box = new Rectangle(15, 20, Color.RED);
        Bouncer bouncer = new Bouncer(100, 170, box);
        bouncer.setMovementVector(3, 1);
        drawable.add(bouncer);
        
        Oval circle = new Oval(30, 15, Color.GREEN);
        bouncer = new Bouncer(20, 20, circle);
        bouncer.setMovementVector(1, 3);
        drawable.add(bouncer);
        
        Rectangle newbox = new Rectangle(40, 20, Color.RED);
        StraightMover mover = new StraightMover(120, 80, newbox);
        mover.setMovementVector(2, 1);
        drawable.add(mover);
        
        Oval newcircle = new Oval(50, 45, Color.GREEN);
        mover = new StraightMover(45, 60, newcircle);
        mover.setMovementVector(2, 4);
        drawable.add(mover);
    }

    /** Draw the contents of the window on surface. */
    public void draw(Graphics surface) {
        for(Mover mover : drawable) {
    	    mover.draw(surface);
        }
    }
}
