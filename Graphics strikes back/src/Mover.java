import java.awt.Graphics;

public interface Mover {
    /**
     * Draws this sprite on surface, with the coordinate (leftX, topY) as the
     * top left corner.
     */
    //void draw(Graphics surface, int leftX, int topY);

    /** Returns the width of the sprite. */
    //int getWidth();

    /** Returns the height of the sprite. */
    //int getHeight();
	
	void setMovementVector(int xIncrement, int yIncrement);
	
	void draw(Graphics surface);
}
