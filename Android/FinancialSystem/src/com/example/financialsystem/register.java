package com.example.financialsystem;

import com.example.financialsystem.R;
import com.example.financialsystem.MainActivity;
import com.example.financialsystem.register;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class register extends Activity{
	
	private RadioGroup radioGroup=null;
	private RadioButton radioButton_student,radioButton_employ,radioButton_assisdent;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.register);
        
        // Initial Database
     	final userinformationDB userHelper = new userinformationDB( this );
        //userHelper = new userinformationDB( this );

        Button EnterRegister = (Button)findViewById(R.id.registerbutton); 
		EnterRegister.setOnClickListener(new Button.OnClickListener() 
    	{
			String account = null;
			String password = null;
			String name = null;
			String email = null;
			String type = null;
			int salary = 0;
			
			@Override
    		public void onClick(View v) 
    		{
				EditText inputNumberLocal1 = (EditText) findViewById(R.id.accTxt);
    		    account = inputNumberLocal1.getText().toString();
    		    
    		    EditText inputNumberLocal2 = (EditText) findViewById(R.id.passTxt);
    		    password = inputNumberLocal2.getText().toString();
    		    
    		    EditText inputNumberLocal3 = (EditText) findViewById(R.id.nameTxt);
    		    name = inputNumberLocal3.getText().toString();
    		    
    		    EditText inputNumberLocal4 = (EditText) findViewById(R.id.emailTxt);
    		    email = inputNumberLocal4.getText().toString();
    		    
    		    RadioGroup Type = (RadioGroup) findViewById(R.id.radioGroup1);
    		    int id = Type.getCheckedRadioButtonId();
    		    
    		    if( id == R.id.radioButton1 )
    		    {
    		    	type = "學生";
    		    }
    		    else if(  id == R.id.radioButton2 )
    		    {
    		    	type = "助教";
    		    }
    		    else if(  id == R.id.radioButton3 )
    		    {
    		    	type = "教職員";
    		    }
    		    /*
    		    int id = Type.getCheckedRadioButtonId();
    		    RadioButton inputNumberLocal5 = (RadioButton) findViewById(id);
    		    if( inputNumberLocal5.getText().toString() == "學生" )
    		    type =  "學生";
    		    */
    		    
				// ---------------- 註冊參數導入 ---------------- // 
    		    SQLiteDatabase userDB = userHelper.getWritableDatabase();
    		    
    		    Cursor userCursor = userDB.rawQuery( "SELECT * FROM UserInformation " +
						                             "WHERE _USER_ACCOUNT = '"+ account +"' " , null );
    		    userCursor.moveToFirst();
    		    
    		   if( userCursor.getCount() == 0 )
    		   {
    	        ContentValues values = new ContentValues();
    	        
    	        values.put("_USER_ACCOUNT", account );
    	        values.put("_USER_PASSWORD",password);
    	        values.put("_USER_NAME",name);
    	        values.put("_USER_EMAIL",email);
    	        values.put("_USER_TYPE", type);
    	        values.put("_USER_SALARY", salary);
    	        
    	        userDB.insertOrThrow("UserInformation",null,values);
    	        userDB.close();
    	        
    	        // TODO Auto-generated method stub 
        		Intent intent = new Intent();
        		intent.setClass(register.this, MainActivity.class);
        		
        		startActivity(intent); 
        		register.this.finish();
    	        
    		   }
    		   else
    		   {
    			   TextView txv = ( TextView ) findViewById(R.id.identicalAccount);
   		    	   txv.setTextColor(Color.RED);
   		    	   txv.setText("*帳號已經存在!!");
    		   }
    		  
    		}
			
    	});	
		
        // 返回
    	Button backButtonRegisterBack = (Button)findViewById(R.id.registerback); 
    	backButtonRegisterBack.setOnClickListener(new Button.OnClickListener()
    	{	
    		@Override
    		public void onClick(View v) 
    		{
    		// TODO Auto-generated method stub 
    		Intent intent = new Intent();
    		intent.setClass(register.this, MainActivity.class);
    		
    		startActivity(intent); 
    		register.this.finish(); 
    		}
    	} );
    }

}
