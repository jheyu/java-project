package com.example.financialsystem;

import com.example.financialsystem.R;
import com.example.financialsystem.userinformationDB;
import com.example.financialsystem.GlobalVariable;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class personal extends Activity{
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.personal);
        
        String email = null;
        String name = null;
        String type = null;
        
		// ------------ 接收Bundle------------ //
		Bundle bundlePERSONAL = this.getIntent().getExtras();
		String account = bundlePERSONAL.getString("account");
		// ------------ 接收Bundle------------ //
		
		
		
		// Init
		userinformationDB userHelper = new userinformationDB(this);
		SQLiteDatabase userDB = userHelper.getWritableDatabase();
		
		
			 
		Cursor userCursor = userDB.rawQuery( "SELECT * FROM UserInformation " +
													 "WHERE _USER_ACCOUNT = '"+ account +"' " , null );
		
		if(userCursor.moveToFirst())
		{
		account = userCursor.getString(1);
		name = userCursor.getString(3);
		email = userCursor.getString(4);
		type = userCursor.getString(5);
		
		TextView showAccount=(TextView)findViewById(R.id.accountTxt);
        showAccount.setText(account);
		
		TextView showName=(TextView)findViewById(R.id.nameTxt);
        showName.setText(name);
        
        TextView showEmail=(TextView)findViewById(R.id.emailTxt);
        showEmail.setText(email);
		     
        TextView showUserType=(TextView)findViewById(R.id.positionTxt);
        showUserType.setText(type);

        
        // 讀取GlobalVariable --> 將登入資訊改為 "已登入" , "傳入帳號資訊"
     	GlobalVariable loginInformation = ( ( GlobalVariable )getApplicationContext() ); 
     	loginInformation.now_isLogin = true;
     	loginInformation.now_account = account;
		}
        // 返回
    	Button backButtonPersonalBack = (Button)findViewById(R.id.personalback); 
    	backButtonPersonalBack.setOnClickListener(new Button.OnClickListener()
    	{
    		@Override
    		public void onClick(View v) 
    		{
    		   // TODO Auto-generated method stub 
    		   Intent intent = new Intent();
    		   intent.setClass(personal.this, MainActivity.class);
    		
    		   startActivity(intent); 
    		   personal.this.finish(); 
    		}
    	} );
    	
    }
    
}