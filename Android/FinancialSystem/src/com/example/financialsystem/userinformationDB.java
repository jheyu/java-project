/*******************************************************************************************
 * userInformationDB.java | 設定SQLite的格式、欄位
*******************************************************************************************/
package com.example.financialsystem;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View.OnClickListener;

public class userinformationDB extends SQLiteOpenHelper {

	final private static int _DB_VERSION = 1;
	final private static String _DB_DATABASE_NAME = "UserInformationDB.db";
	
		
	public userinformationDB(Context context)
	{
		super(context,_DB_DATABASE_NAME,null,_DB_VERSION);
	}
	
	public userinformationDB(Context context, String name, CursorFactory factory, int version)
	{
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(SQLiteDatabase db)
	{		
		// TODO Auto-generated method stub
		db.execSQL
		(
			"CREATE TABLE UserInformation (" +
		
			// Implement in user_information.java									
			//======================================================================================	
			" _id INTEGER PRIMARY KEY, " +         // user id
			" _USER_ACCOUNT VARCHAR(50), " +		// user Account								
			" _USER_PASSWORD VARCHAR(50), " +		// user password							
			" _USER_NAME VARCHAR(50), " + 	// user name								
			" _USER_EMAIL VARCHAR(50), " +		// user email							     		 
			" _USER_TYPE VARCHAR(50), " +		// user type      
			" _USER_SALARY INTEGER " +
	        //======================================================================================
			
	        ")"
		);
		/*
		String manager = "INSERT INTO UserInformation (_USER_ACCOUNT) VALUES('test1')";
		manager = "INSERT INTO UserInformation (_USER_PASSWORD) VALUES('test1')";
		manager = "INSERT INTO UserInformation (_USER_NAME) VALUES('systemManager')";
		manager = "INSERT INTO into UserInformation (_USER_EMAIL) VALUES('gg')";
		manager = "INSERT INTO UserInformation (_USER_TYPE) VALUES('管理者')";
		manager = "INSERT INTO UserInformation (_USER_SALARY) VALUES('0')";
		
	    db.execSQL(manager);
		*/
		ContentValues values = new ContentValues();
		values.put("_USER_ACCOUNT", "test1");
		values.put("_USER_PASSWORD", "test1");
		values.put("_USER_NAME", "systemManager");
		values.put("_USER_EMAIL", "gg");
		values.put("_USER_TYPE", "管理者");
		values.put("_USER_SALARY", "0");
		
		db.insertOrThrow("UserInformation", null, values);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS UserInformation");
		onCreate(db);
	}
	
}