package com.example.financialsystem;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class salary extends Activity{
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.salary);
        
        // Initial Database
     	final userinformationDB userHelper = new userinformationDB( this );
        
        // 查詢薪資
    	Button searchSalaryButton = (Button)findViewById(R.id.searchSalary); 
    	searchSalaryButton.setOnClickListener(new Button.OnClickListener()
    	{
            SQLiteDatabase userDB = userHelper.getWritableDatabase();
            String account = null;
			String name = null;
			
    		
    		@Override
    		public void onClick(View v) 
    		{
    		   EditText accountText = (EditText)findViewById(R.id.chooseItem);
    		   account = accountText.getText().toString();
    			
    		   EditText nameText = (EditText)findViewById(R.id.accTxt);
    		   name = nameText.getText().toString();
    		   
    		   
    		   Cursor userCursor = userDB.rawQuery( "SELECT * FROM UserInformation " +
						 " WHERE _USER_ACCOUNT = '"+ account +"' " +
						 " AND _USER_NAME = '"+ name +"' " , null );
    		   
              if(userCursor.moveToFirst())
              {
            	  TextView positionTxv = (TextView)findViewById(R.id.positionTxv);
            	  positionTxv.setText(userCursor.getString(5));
            	  TextView salaryTxv = (TextView)findViewById(R.id.salaryTxv);
            	  salaryTxv.setText(userCursor.getString(6));
              }/**/
              else
              {
            	  TextView txv = (TextView)findViewById(R.id.notSearchSalary);
            	  txv.setTextColor(Color.RED);
            	  txv.setText("*查無此人薪水!!");
              }
    		  
    		}
    		
    	} );
       
         // 返回
    	Button backButtonSalaryBack = (Button)findViewById(R.id.salaryBack); 
    	backButtonSalaryBack.setOnClickListener(new Button.OnClickListener()
    	{
    		@Override
    		public void onClick(View v) 
    		{
    		// TODO Auto-generated method stub 
    		Intent intent = new Intent();
    		intent.setClass(salary.this, MainActivity.class);
    		
    		startActivity(intent); 
    		salary.this.finish(); 
    		}
    	} );
    	
    }
    
}