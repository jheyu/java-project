package com.example.financialsystem;

import com.example.financialsystem.R;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class login extends Activity{
	
	private userinformationDB userHelper;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        
        //requestWindowFeature(Window.FEATURE_NO_TITLE); 
	    //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	    //WindowManager.LayoutParams.FLAG_FULLSCREEN);
 
        // Initial Database
     	userinformationDB userHelper = new userinformationDB( this );
     	
        final SQLiteDatabase userDB = userHelper.getWritableDatabase();
        
        // �n�J
        Button Tologin = (Button)findViewById(R.id.loginButton); 
     	Tologin.setOnClickListener(new Button.OnClickListener()
     	{
     		GlobalVariable information = ( ( GlobalVariable )getApplicationContext() );
			String account = null;
			String password = null;
			 
     		@Override
			public void onClick(View v) 
			{
     			EditText accountInput = (EditText) findViewById(R.id.accTxt);
				account = accountInput.getText().toString();
		    
		    	EditText inputPassword = (EditText) findViewById(R.id.passTxt);
		    	password = inputPassword.getText().toString();
		    	
		    	if( account.equals("test1") )
	    		{
	    			information.limits_ToAuthority = 1;
	    		}
		    	
		    	Cursor userCursor = userDB.rawQuery( "SELECT * FROM UserInformation " +
						 " WHERE _USER_ACCOUNT = '"+ account +"' " +
						 " AND _USER_PASSWORD = '"+ password +"' " , null );
		    	
                userCursor.moveToFirst();
                 
                if( userCursor.getCount() != 0 )  // �b�K���T
				{
                	
					Intent intent = new Intent();
					intent.setClass(login.this, personal.class);
		            
					// ��Bundle�hlogin����
		    		Bundle accountInformationBundle = new Bundle(); 
		    		accountInformationBundle.putString("account", account);
		    		intent.putExtras(accountInformationBundle);  
		    		
					startActivity(intent); 
					login.this.finish();
				}
                else // �b�K���~
				{
			        TextView state=(TextView)findViewById(R.id.errorTxt);
			        state.setTextColor(Color.RED);
			        state.setText("*���~���b���αK�X");
				}
                
                /*
                if( userCursor.getCount() == 0)
                {
                    Intent intent = new Intent();
				    intent.setClass(login.this, MainActivity.class);
				    startActivity(intent); 
				    login.this.finish();
                }*/
			}
     		
     	} );
     	
        
        // ��^
    	Button backButtonLoginBack = (Button)findViewById(R.id.loginback); 
    	backButtonLoginBack.setOnClickListener(new Button.OnClickListener()
    	{
    		@Override
    		public void onClick(View v) 
    		{
    		   // TODO Auto-generated method stub 
    		   Intent intent = new Intent();
    		   intent.setClass(login.this, MainActivity.class);
    		
    		   startActivity(intent); 
    		   login.this.finish(); 
    		}
    		
    	} );
    	
    }
    
}