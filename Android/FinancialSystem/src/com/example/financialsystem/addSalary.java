package com.example.financialsystem;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class addSalary extends Activity {

	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.addsalary);
        
        
        // Initial Database
     	userinformationDB userHelper = new userinformationDB( this );
     	
        final SQLiteDatabase userDB = userHelper.getWritableDatabase();
     	
        // 配給
    	Button add = (Button)findViewById(R.id.addMoneyToWho); 
    	add.setOnClickListener(new Button.OnClickListener()
    	{
    		String account = null;
    		String name = null;
			int salary = 0;
    		
    		@Override
    		public void onClick(View v) 
    		{
    		   // TODO Auto-generated method stub 
    		   EditText useraccount = (EditText) findViewById(R.id.addToAccountText);
    		   account = useraccount.getText().toString();	
    			
    		   EditText username = (EditText) findViewById(R.id.addToWhoText);
    		   name = username.getText().toString();
    		   
    		   EditText money = (EditText) findViewById(R.id.addMoneyText);
    		   salary = Integer.parseInt(money.getText().toString());
    		   
   		    
   		       Cursor userCursor = userDB.rawQuery( "SELECT * FROM UserInformation " +
						                             "WHERE _USER_ACCOUNT = '"+ account +"' " +
						            				 " AND _USER_NAME = '"+ name +"' "  , null );
   		       
   		       if( userCursor.moveToFirst() )
   		       {
   		    	      		    	 
   		    	 ContentValues values = new ContentValues(); 
   		    	 values.put("_USER_ACCOUNT", account);
   		    	 values.put("_USER_NAME", name);
   		    	 values.put("_USER_SALARY", salary);
   		    	
    	         userDB.update("userinformation", values , "_USER_ACCOUNT = '"+ account +"' " + 
    	                                                     " AND _USER_NAME = '"+ name +"' ", null);
    	         userDB.close(); 
    		      
    		     Intent intent = new Intent();
    		     intent.setClass(addSalary.this, MainActivity.class);
    		     
    		     startActivity(intent); 
    		     addSalary.this.finish(); 
   		       }
   		       else
   		       {
   		    	 TextView txv=(TextView)findViewById(R.id.notExist);
   		    	 txv.setTextColor(Color.RED);
   		         txv.setText("*查無此人!!");
   		       }
   		       
    		}
    	} );
        
        
        // 返回
    	Button backButtonaddSalaryBack = (Button)findViewById(R.id.labreturnbutton); 
    	backButtonaddSalaryBack.setOnClickListener(new Button.OnClickListener()
    	{
    		@Override
    		public void onClick(View v) 
    		{
    		   // TODO Auto-generated method stub 
    		   Intent intent = new Intent();
    		   intent.setClass(addSalary.this, MainActivity.class);
    		
    		   startActivity(intent); 
    		   addSalary.this.finish(); 
    		}
    	} );
        
    }
}
