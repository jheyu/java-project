package com.example.financialsystem;

import com.example.financialsystem.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.main);
        
             
	    // ----------- 按鈕----------- //
    	Button reigster = (Button)findViewById(R.id.registerbutton); 
    	reigster.setOnClickListener(new Button.OnClickListener()
    	{
    		@Override
    		public void onClick(View v) 
    		{
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, register.class);
				
				startActivity(intent); 
				MainActivity.this.finish(); 
    		}
    		
    	} );
    	
    	// ----------- 按鈕----------- //
        Button login = (Button)findViewById(R.id.loginbutton);      
        login.setOnClickListener(new Button.OnClickListener()
		{		
			// 讀取GlobalVariable --> 為了拿取登入資訊
			GlobalVariable information = ( ( GlobalVariable )getApplicationContext() ); 
			
			@Override
			public void onClick(View v) 
			{	
				if ( information.now_isLogin == false )  // 尚未登入
				{
					// TODO Auto-generated method stub 
					Intent intent = new Intent();
					intent.setClass(MainActivity.this, login.class);
					
					startActivity(intent); 
					MainActivity.this.finish();  
				}
				else  // 登入狀態
				{
			        TextView state=(TextView)findViewById(R.id.mainState);
			        state.setTextColor(Color.RED);
			        state.setText("*您已經以 "+information.now_account+" 的身分登入");
				}
			}
		} );
        
    	/*
    	// ----------- 按鈕----------- //
    	Button login = (Button)findViewById(R.id.loginbutton); 
    	login.setOnClickListener(new Button.OnClickListener()
    	{
    		@Override
    		public void onClick(View v) 
    		{
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, login.class);
				
				startActivity(intent); 
				MainActivity.this.finish(); 
    		}
    		
    	} );
    	*/
    	// ----------- 按鈕----------- //
    	Button personal = (Button)findViewById(R.id.personalbutton); 
    	personal.setOnClickListener(new Button.OnClickListener()
    	{
    		// 讀取GlobalVariable --> 為了拿取登入資訊
    					GlobalVariable information = ( ( GlobalVariable )getApplicationContext() ); 
    					
    					@Override
    					public void onClick(View v) 
    					{	
    						if ( information.now_isLogin == false )  // 尚未登入
    						{
    							
    							 TextView state=(TextView)findViewById(R.id.mainState);
    						     state.setTextColor(Color.RED);
    						     state.setText("*尚未登入,請先登入");
    						}
    						else  // 登入狀態
    						{
    							// TODO Auto-generated method stub 
    							Intent intent = new Intent();
    							intent.setClass(MainActivity.this, personal.class);
    							
    							startActivity(intent); 
    							MainActivity.this.finish(); 
    						}
    					}
    		
    	} );
    	
    	// ----------- 按鈕----------- //
    	Button salary = (Button)findViewById(R.id.salarybutton); 
    	salary.setOnClickListener(new Button.OnClickListener()
    	{
    		// 讀取GlobalVariable --> 為了拿取登入資訊
    					GlobalVariable information = ( ( GlobalVariable )getApplicationContext() ); 
    					
    					@Override
    					public void onClick(View v) 
    					{	
    						if ( information.now_isLogin == false )  // 尚未登入
    						{
    							
    							TextView state=(TextView)findViewById(R.id.mainState);
   						        state.setTextColor(Color.RED);
   						        state.setText("*尚未登入,請先登入");
    						 
    						}
    						else  // 登入狀態
    						{
    							// TODO Auto-generated method stub 
    							Intent intent = new Intent();
    							intent.setClass(MainActivity.this, salary.class);
    							
    							startActivity(intent); 
    							MainActivity.this.finish(); 
    						}
    					}
    		
    	} );
     	
    	// ----------- 按鈕----------- //
    	Button laboratory = (Button)findViewById(R.id.laboratorybutton); 
    	laboratory.setOnClickListener(new Button.OnClickListener()
    	{
    		// 讀取GlobalVariable --> 為了拿取登入資訊
    					GlobalVariable information = ( ( GlobalVariable )getApplicationContext() ); 
    					
    					@Override
    					public void onClick(View v) 
    					{	
    						if ( information.now_isLogin == false )  // 尚未登入
    						{
    							
    							TextView state=(TextView)findViewById(R.id.mainState);
   						        state.setTextColor(Color.RED);
   						        state.setText("*尚未登入,請先登入");
    							  
    						}
    						else  // 登入狀態
    						{
    							// TODO Auto-generated method stub 
    							Intent intent = new Intent();
    							intent.setClass(MainActivity.this, laboratory.class);
    							
    							startActivity(intent); 
    							MainActivity.this.finish();
    						}
    					}
    		
    	} );
    	
    	// ----------- 按鈕前往EXIT----------- //
    	Button EXIT = (Button)findViewById(R.id.button_EXIT); 
    	EXIT.setOnClickListener(new Button.OnClickListener()
    	{
    		@Override
    		public void onClick(View v) 
    		{
    			// 讀取GlobalVariable --> 為了拿取登入資訊
    			GlobalVariable information = ( ( GlobalVariable )getApplicationContext() ); 
    			information.now_isLogin = false;
    			information.limits_ToAuthority = 0;
    			// Logout
    			//GlobalVariable loginInformation = ( ( GlobalVariable )getApplicationContext() ); 
    			//loginInformation.now_isLogin = false;
    			//loginInformation.now_account = null;
    			MainActivity.this.finish(); 
    			//System.exit(0);//關閉程式語法
    		}
    	} );
    	
    	 // ----------- 按鈕----------- //
    	Button addSalary = (Button)findViewById(R.id.addSalay); 
    	addSalary.setOnClickListener(new Button.OnClickListener()
    	{
    		@Override
    		public void onClick(View v) 
    		{
    			GlobalVariable information = ( ( GlobalVariable )getApplicationContext() ); 
    			if( information.limits_ToAuthority == 1  )
    			{
				   Intent intent = new Intent();
				   intent.setClass(MainActivity.this, addSalary.class);
				
				   startActivity(intent); 
				   MainActivity.this.finish(); 
    			}
    			else
    			{
    				TextView txv = (TextView) findViewById(R.id.limitText);
    				txv.setTextColor(Color.RED);
    		    	txv.setText("*你沒有權限使用!!");
    				
    			}
    		}
    		
    	} );
        
    }

}
