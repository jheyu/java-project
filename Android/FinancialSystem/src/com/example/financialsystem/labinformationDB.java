/* labInformationDB.java | 設定SQLite的格式、欄位
/*******************************************************************************************/
package com.example.financialsystem;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View.OnClickListener;

public class labinformationDB extends SQLiteOpenHelper {

	final private static int _DB_VERSION = 1;
	final private static String _DB_DATABASE_NAME = "labInformationDB.db";
	
		
	public labinformationDB(Context context)
	{
		super(context,_DB_DATABASE_NAME,null,_DB_VERSION);
	}
	
	public labinformationDB(Context context, String name, CursorFactory factory, int version)
	{
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(SQLiteDatabase db)
	{		
		// TODO Auto-generated method stub
		db.execSQL
		(
			"CREATE TABLE labInformation (" +
		
			// Implement in lab_information.java									
			//======================================================================================
			" _id INTEGER PRIMARY KEY," +
			" item VARCHAR(50), " + 			
			" count INTEGER " +                
	        //======================================================================================
			
	        ")"
		);
		
        
	    ContentValues values1 = new ContentValues();
		values1.put("item", "電源供應器");//電源供應器
		values1.put("count", "10");
		db.insertOrThrow("labInformation", null, values1);
		
		ContentValues values2 = new ContentValues();
		values2.put("item", "訊號產生器");//訊號產生器
		values2.put("count", "10");
		db.insertOrThrow("labInformation", null, values2);
		
		ContentValues values3 = new ContentValues();
		values3.put("item", "示波器");//示波器
		values3.put("count", "10");
		db.insertOrThrow("labInformation", null, values3);
 
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS labInformation");
		onCreate(db);
	}
	
}