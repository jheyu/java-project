/*******************************************************************************************
 * GlobalVariable.java | 全域變數
*******************************************************************************************/
package com.example.financialsystem;

import android.app.Application;

public class GlobalVariable extends Application
{
	
	// 必要
	public boolean now_isLogin = false;
	public String now_account = null;
	public int limits_ToAuthority = 0;
	/*
	// 次要
	public String now_reason = null;
	public String now_startTime = null;
	// 價錢相關(小鴨公)
	public String now_brand = null;
	public int now_price = 0;
	public int now_day_amount = 0;
	// 小鴨養成系統
	public int now_weight = 0;
	*/
}
