package com.example.financialsystem;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class laboratory extends Activity{
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.laboratory);
      
        // Initial Database
        labinformationDB labHelper = new labinformationDB( this );
     	
        final SQLiteDatabase labDB = labHelper.getWritableDatabase();
     
        
        Cursor labCursor1 = labDB.rawQuery( "SELECT * FROM labInformation " +
                " WHERE item = '電源供應器' " , null );
        labCursor1.moveToFirst();
     	TextView count1 = (TextView)findViewById(R.id.count1);
     	count1.setText(labCursor1.getString(2)+" 臺");
     	
     	Cursor labCursor2 = labDB.rawQuery( "SELECT * FROM labInformation " +
                "WHERE item = '訊號產生器' " , null );
     	labCursor2.moveToFirst();
     	TextView count2 = (TextView)findViewById(R.id.count2);
     	count2.setText(labCursor2.getString(2)+" 臺");
     	
     	Cursor labCursor3 = labDB.rawQuery( "SELECT * FROM labInformation " +
                "WHERE item = '示波器' " , null );
     	labCursor3.moveToFirst();
     	TextView count3 = (TextView)findViewById(R.id.count3);
     	count3.setText(labCursor3.getString(2)+" 臺");
     	
        // 租借
    	Button labBorrow = (Button)findViewById(R.id.borrowItem); 
    	labBorrow.setOnClickListener(new Button.OnClickListener()
    	{
            
    		String item = null;
    		int count = 0;
    		@Override
    		public void onClick(View v) 
    		{
                 /*
    	     	EditText chooseItem = (EditText)findViewById(R.id.chooseItem);  
                item = chooseItem.getText().toString();
                
                EditText countNumber = (EditText)findViewById(R.id.countNumber);  
                count = Integer.parseInt(countNumber.getText().toString());
              
    	     	Cursor labCursor = labDB.rawQuery( "SELECT * FROM labInformation " +
                       "WHERE item = '"+ item +"' " , null );
    	     	 
    	     	labCursor.moveToFirst();
    	        
    	     	if( Integer.parseInt(labCursor.getString(2)) - count >= 0 )
    	     	{
    	     	ContentValues values = new ContentValues();
	     		values.put( "item" , item );
	     		values.put( "count" , String.valueOf( Integer.parseInt(labCursor.getString(1)) - count ) );
	     		labDB.update("labinformation", values , "item = '" + item + "'" 
	     				     , null);
	     	
	     		
    	     	if( labCursor.getString(1).equals("letter") )
    	     	{
    	     		Cursor labCursor1 = labDB.rawQuery( "SELECT * FROM labInformation " +
    	                    "WHERE item = 'letter' " , null );
    	            labCursor1.moveToFirst();
    	         	TextView count1 = (TextView)findViewById(R.id.count1);
    	         	count1.setText(labCursor1.getString(2)+"臺");
    	     	}
    	     	else if( labCursor.getString(1).equals("訊號產生器") )
    	     	{
    	         	Cursor labCursor2 = labDB.rawQuery( "SELECT * FROM labInformation " +
    	                    "WHERE item = '訊號產生器' " , null );
    	         	labCursor2.moveToFirst();
    	         	TextView count2 = (TextView)findViewById(R.id.count2);
    	         	count2.setText(labCursor2.getString(2)+"臺");
    	     	}
    	     	else if( labCursor.getString(1).equals("示波器") )
    	     	{
    	     		Cursor labCursor3 = labDB.rawQuery( "SELECT * FROM labInformation " +
    	                    "WHERE item = '示波器' " , null );
    	         	labCursor3.moveToFirst();
    	         	TextView count3 = (TextView)findViewById(R.id.count3);
    	         	count3.setText(labCursor3.getString(2)+"臺");
    	     	}
    	     	}*/
    	     	//else
    	     	
    	        //labDB.close();	
    			
    			/*
    		    // TODO Auto-generated method stub 
    		    Intent intent = new Intent();
    		    intent.setClass(laboratory.this, MainActivity.class);
    		
    		    startActivity(intent); 
    		    laboratory.this.finish(); 
    		    */
    		    
    		}
    	} );
    	
    	// 歸還
    	Button returnItem = (Button)findViewById(R.id.returnItem); 
    	returnItem.setOnClickListener(new Button.OnClickListener()
    	{
    		@Override
    		public void onClick(View v) 
    		{
    		// TODO Auto-generated method stub 
    		Intent intent = new Intent();
    		intent.setClass(laboratory.this, MainActivity.class);
    		
    		startActivity(intent); 
    		laboratory.this.finish(); 
    		}
    	} );
    	
        
        // 返回
    	Button backButtonLaboratoryBack = (Button)findViewById(R.id.labreturnbutton); 
    	backButtonLaboratoryBack.setOnClickListener(new Button.OnClickListener()
    	{
    		@Override
    		public void onClick(View v) 
    		{
    		// TODO Auto-generated method stub 
    		Intent intent = new Intent();
    		intent.setClass(laboratory.this, MainActivity.class);
    		
    		startActivity(intent); 
    		laboratory.this.finish(); 
    		}
    	} );
    	
    }
    
}