package com.example.internetsurfer;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.widget.Toast;
import android.content.Context;
import android.media.SoundPool;

public class MainActivity extends Activity {
    private ImageButton previousBtn,nextBtn,clearBtn,reloadBtn,playBtn,stopBtn;
    private EditText addressTxt;
    private WebView webView;
    //private MediaPlayer mediaPlayer;
    private Toast toast;
    private int alert;
    private SoundPool sound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
		System.out.println("aa");
        previousBtn=(ImageButton)findViewById(R.id.previous);
        previousBtn.setVisibility(0);
        previousBtn.setOnClickListener(backListener);
        nextBtn=(ImageButton)findViewById(R.id.next);
        nextBtn.setVisibility(0);
        nextBtn.setOnClickListener(nextListener);
        clearBtn=(ImageButton)findViewById(R.id.clear);
        clearBtn.setVisibility(0);
        clearBtn.setOnClickListener(clearListener);
        reloadBtn=(ImageButton)findViewById(R.id.reload);
        reloadBtn.setVisibility(0);
        reloadBtn.setOnClickListener(reloadListener);/*
        playBtn=(ImageButton)findViewById(R.id.play);
        playBtn.setVisibility(0);
        playBtn.setOnClickListener(playListener);
        stopBtn=(ImageButton)findViewById(R.id.stop);
        stopBtn.setVisibility(0);
        stopBtn.setOnClickListener(stopListener);*/
        addressTxt=(EditText)findViewById(R.id.addressTxt);
        addressTxt.setOnKeyListener(addressListener);
        webView=(WebView)findViewById(R.id.webview);
        webView.setWebViewClient(new MyWebViewClient());
        webView.loadUrl("http://tw.msn.com/?ocid=iefvrt");
        //mediaPlayer=MediaPlayer.create(this, R.drawable.sleepaway);
        sound = new SoundPool(1, AudioManager.STREAM_MUSIC, 5);
        alert = sound.load(this, R.drawable.click, 1);
    }
    private class MyWebViewClient extends WebViewClient {
        @Override
        public void doUpdateVisitedHistory(WebView myWebView,String url,boolean isReload) {
             if(myWebView.canGoBack())
            	 previousBtn.setVisibility(0);
             if(myWebView.canGoForward())
            	 nextBtn.setVisibility(0);
        }
    };
    private ImageButton.OnClickListener backListener=new ImageButton.OnClickListener() {
    	public void onClick(View view) {
    		webView.goBack();
    		sound.play(alert, 1, 1, 0, 0, 1);
    	}
    };
    private ImageButton.OnClickListener nextListener=new ImageButton.OnClickListener() {
    	public void onClick(View view) {
    		webView.goForward();
    		sound.play(alert, 1, 1, 0, 0, 1);
    	}
    };
    private ImageButton.OnClickListener clearListener=new ImageButton.OnClickListener() {
    	public void onClick(View view) {
    		webView.clearHistory();
    		
    		sound.play(alert, 1, 1, 0, 0, 1);
    		Context context = getApplication();
            CharSequence text = "clear history";
            int duration = Toast.LENGTH_SHORT;
            Toast.makeText(context, text, duration).show();
    	}
    };
    private ImageButton.OnClickListener reloadListener=new ImageButton.OnClickListener() {
    	public void onClick(View view) {
    		webView.reload();
    		
    		sound.play(alert, 1, 1, 0, 0, 1);
    		Context context = getApplication();
            CharSequence text = "page reload";
            int duration = Toast.LENGTH_SHORT;
            Toast.makeText(context, text, duration).show();
    	}
    };/*
    private ImageButton.OnClickListener playListener=new ImageButton.OnClickListener() {
    	public void onClick(View view) {
    		mediaPlayer.start();
    		
    		Context context = getApplication();
            CharSequence text = "music start";
            int duration = Toast.LENGTH_SHORT;
            Toast.makeText(context, text, duration).show();
    	}
    };
    private ImageButton.OnClickListener stopListener=new ImageButton.OnClickListener() {
    	public void onClick(View view) {
    		mediaPlayer.pause();
    		
    		Context context = getApplication();
            CharSequence text = "music pause";
            int duration = Toast.LENGTH_SHORT;
            Toast.makeText(context, text, duration).show();
    	}
    };*/
    private EditText.OnKeyListener addressListener=new EditText.OnKeyListener() {
    	    public boolean onKey(View view, int keyCode, KeyEvent keyevent) {
    	    	if(keyCode==KeyEvent.KEYCODE_ENTER) {
    	    		System.out.println("test");
    	    		webView.loadUrl(addressTxt.getEditableText().toString());
    	    		return true;
    	    	}
    	    	return false;
    	}
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
