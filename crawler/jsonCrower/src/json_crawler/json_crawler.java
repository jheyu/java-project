package json_crawler;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;  
import java.util.Timer;  
import java.util.TimerTask;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import json_crawler.mysql_config;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class json_crawler {
    
	private static Connection con = null;
	private static Statement stat = null;
	
	Timer timer;
	mysql_config myconfig = new mysql_config();

	public json_crawler(int firstTime,int period) {
		
		timer = new Timer();
    	timer.schedule(new Task(), firstTime*1000, period*1000);
	}
	
	class Task extends TimerTask {
	    	// overwrite
	        public void run() {
	        	String url = "http://opendata.epa.gov.tw/ws/Data/RainTenMin/?format=json";
	        	String jsonContent = GetJsonContent(url);
	        	String insertSQL = GetInsertSQL(jsonContent); 
	        	//timer.cancel(); //Terminate the timer thread
	        }  
	}  
	
    public static void main(String[] args) {
    	try {
            // TODO code application logic here
    		json_crawler app = new json_crawler();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(json_crawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    	System.out.println("Task scheduled...");
    	new json_crawler(1,600);// delay 1s and update per 10s
    }
    
    public json_crawler() throws ClassNotFoundException {
        String sql = "show tables;";
        this.mysql(sql);
    }
    
    public static void jdbcmysql(String sql)
    {
    	System.out.println("jdbcmysql...");
    	try {
    	  Class.forName("com.mysql.jdbc.Driver");
    	  con = DriverManager.getConnection( 
    		      "jdbc:mysql://140.138.77.103/BDSTeam30_DB?useUnicode=true&characterEncoding=big5", 
    		      "BDSTeam30","BDSTeam30@2015"); 
    		      //���oconnection
    	  stat = con.createStatement(); 
          stat.executeUpdate(sql);
          
          stat.close();
          con.close();
    	}
    	catch(ClassNotFoundException e)
    	{
    		System.out.println("DriverClassNotFound :"+e.toString()); 
    	}
    	catch(SQLException x) { 
    	      System.out.println("Exception :"+x.toString()); 
    	} 
    }
    
    public String mysql(String sql) throws ClassNotFoundException {
        String str = "";
        sql = sql.replace("--", "");
        Connection con = null;
        try {
            Class.forName(myconfig.getMysql_DRIVER_NAME());
            con = DriverManager.getConnection(myconfig.getCONNECTION_URL_MYSQL(), myconfig.getMYSQLUSER(), myconfig.getMYSQLUSERPWD());
            try (Statement stmt = con.createStatement()) {
                ResultSet rs = stmt.executeQuery(sql);
                ResultSetMetaData rsmd = rs.getMetaData();
                int col = rsmd.getColumnCount();
                // print the results to the console
                while (rs.next()) {

                    for (int i = 1; i <= col; i++) {
                        System.out.println(rs.getString(i) );
                    }
                }
//                str = mJsonArray.toString();
                rs.close();
                stmt.close();
            }
            con.close();
        } catch (SQLException e) {

            System.out.println(e);
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                // swallow
            }
        }
        return str;
    }
    
    static String GetJsonContent(String url) {
    	
    	String jsonContent = "";
    	try{
    		URL pageUrl = new URL(url);
    		HttpURLConnection huc = (HttpURLConnection) pageUrl.openConnection();
    		StringBuffer sb = new StringBuffer();
    		InputStreamReader ir = new InputStreamReader(huc.getInputStream(), "UTF-8");
    		BufferedReader br = new BufferedReader(ir);
    		String content = "";

    		while ((content = br.readLine()) != null)
                sb.append(content);
    		
    		jsonContent = sb.toString();
    		
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    	}
    	
    	return jsonContent;
    }
    
    static String GetInsertSQL(String jsonstr) {

    	String sql = "";
        JSONArray jsonArray = new JSONArray(jsonstr);
        //System.out.println(jsonArray.get(1));

        try {

            String tags = "";
            String values = "";
            JSONObject jsonObject = new JSONObject();
            
            jsonObject = jsonArray.getJSONObject(0);
            //System.out.println("JSON Object : " + jsonObject);
            //System.exit(0);
            JSONArray jsonname = new JSONArray();
            jsonname = jsonObject.names();
            //System.out.println("Key Array : " + jsonname);
            //System.exit(0);

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);

                String invalue = "";

                for (int k = 0; k < jsonname.length(); k++) {

                    invalue = invalue + "'" + jsonObject.get(jsonname.get(k).toString()) + "',";
                }
                invalue = invalue.substring(0, invalue.length() - 1);
                //System.out.println(invalue);
                values = values + "(" + invalue + ")," + "\n";
            }
            values = values.substring(0, values.length() - 2);
            //System.out.println(values);
            
            for (int i = 0; i < jsonname.length(); i++) {
            	tags = tags + jsonname.get(i) + ",";
            }
            tags = tags.substring(0, tags.length() - 1);
            System.out.println(tags);
            sql = "INSERT INTO rainData (" + tags + ") VALUES " + values + ";";
            //app.mysql(sql);
            //InsertMySQL();
            jdbcmysql(sql);
            System.out.println(sql);
             
        } catch (JSONException e) {
        	System.out.println(e.toString());
        }
        return sql;
    }
    
}

