package json_crawler;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author PAN
 */
public class mysql_config {

    //  JDBC host    
    private String _MYSQL_HOST = "140.138.77.103";
    //  JDBC port 
    private String _MYSQL_JDBC_PORT = "3306";
    private String _myDB = "BDSTeam30_DB";
    //  JDBC user password 
    private String _MYSQLUSER = "BDSTeam30";
    private String _MYSQLUSERPWD = "BDSTeam30@2015";
    private String CONNECTION_URL_MYSQL = "jdbc:mysql://" + _MYSQL_HOST + ':' + _MYSQL_JDBC_PORT + "/" + _myDB;
    private String mysql_DRIVER_NAME = "com.mysql.jdbc.Driver";

    public String getMYSQL_HOST() {
        return _MYSQL_HOST;
    }

    public String getMYSQL_JDBC_PORT() {
        return _MYSQL_JDBC_PORT;
    }

    public String getMyDB() {
        return _myDB;
    }

    public String getMYSQLUSER() {
        return _MYSQLUSER;
    }

    public String getMYSQLUSERPWD() {
        return _MYSQLUSERPWD;
    }

    public String getCONNECTION_URL_MYSQL() {
        return CONNECTION_URL_MYSQL;
    }

    public String getMysql_DRIVER_NAME() {
        return mysql_DRIVER_NAME;
    }

    public void setMYSQL_HOST(String _MYSQL_HOST) {
        this._MYSQL_HOST = _MYSQL_HOST;
    }

    public void setMYSQL_JDBC_PORT(String _MYSQL_JDBC_PORT) {
        this._MYSQL_JDBC_PORT = _MYSQL_JDBC_PORT;
    }

    public void setMyDB(String _myDB) {
        this._myDB = _myDB;
    }

    public void setMYSQLUSER(String _MYSQLUSER) {
        this._MYSQLUSER = _MYSQLUSER;
    }

    public void setMYSQLUSERPWD(String _MYSQLUSERPWD) {
        this._MYSQLUSERPWD = _MYSQLUSERPWD;
    }

    public void setCONNECTION_URL_MYSQL(String CONNECTION_URL_MYSQL) {
        this.CONNECTION_URL_MYSQL = CONNECTION_URL_MYSQL;
    }

    public void setMysql_DRIVER_NAME(String mysql_DRIVER_NAME) {
        this.mysql_DRIVER_NAME = mysql_DRIVER_NAME;
    }

}
