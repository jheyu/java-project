package java_crawler;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class java_crawler {
	
	Timer timer;
	public java_crawler(int firstTime,int period) {
		
		timer = new Timer();
		timer.schedule(new Task(), firstTime*1000, period*1000);
	}
 
	class Task extends TimerTask {
 	    // overwrite
        public void run() {
        	
        	try{
        		URL url = new URL("http://reg.ntuh.gov.tw/EmgInfoBoard/Y0NTUHEmgInfo.aspx");
        		/*
        		 * 向網頁伺服發出請求，並將回應分析成document。
        		 * 第一個參數是：請求位置與QueryString。
        		 * 第二個參數是：連線時間(毫秒)，在指定時間內若無回應則會丟出IOException
        		 */
        		Document doc = Jsoup.parse(url, 3000);

        		//取回所center下所有的table
        		Elements tds = doc.select("td");
        		//Elements trs = tables.select("tr");
        		//Elements tds = tables.select("td");
        		//Iterator iterator;
        		// 國立臺灣大學醫學院附設醫院雲林分院
        		String sql = "INSERT INTO National_Taiwan_Yun_Lin_Branch (hospitalname,等候掛號人數,等候看診人數,等候住院人數,等候ICU人數,等候推床人數,目前日期) VALUES (";
        		String str = "";
        		//print all table
        		//System.out.println("total: " + tds);
        		sql += "國立臺灣大學醫學院附設醫院雲林分院,";
        		for(int i=0; i<tds.size()-1;i++)
        		{
        			str = tds.get(i).text().toString();
        			if(str.matches("[0-9]+人"))
        			{
        				//System.out.println("data" + ": " + str.replace("人",""));
        				str = str.replace("人","");
        				sql += str + ",";
        			}
        		}
        		//sql = sql.substring(0,sql.length()-1);
        		
        		// 取得目前時間
        		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        		Date date = new Date();
        		String strDate = sdFormat.format(date);
        		sql += strDate;
        		sql += ");";
        		System.out.println(sql);
        	}
        	catch(Exception e)
        	{
        		System.out.println(e.getMessage());
        	}
        }  
	}  
 
	public static void main(String[] args) {

		System.out.println("Task scheduled...");
		new java_crawler(1,10);// delay 1s and update per 10s
    
	}
}
