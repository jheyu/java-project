import java.io.*;
import java.net.*;
import java.lang.*;

public class Client {
	public static void main(String[] args) {
		String message, stdin, ip;
		int port;
		
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("請輸入IP : ");
			ip = br.readLine();
			System.out.print("請輸入port : ");
			port = Integer.parseInt(br.readLine());
			
			Socket con = new Socket(ip, port);// 建立連線
			if (con.isConnected()) {
				System.out.println("Connect Success");
			
				Thread thread = new Thread(new ClientTalk(con));
				thread.start(); 
				
				DataOutputStream dos = new DataOutputStream(con.getOutputStream());// 建立DataOutputStream將資料寫至Server
				while (true) {
					stdin = br.readLine();
					dos.writeUTF(stdin);
				}
			} else
				System.out.println("Connect Faild");
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
}

class ClientTalk implements Runnable 
{
	DataInputStream input;
	Socket clientSocket;

	public ClientTalk(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	public void run() {
		try {
			input = new DataInputStream(clientSocket.getInputStream());
			while (true) {
				String read = input.readUTF();// 讀取Server端傳送過來的資訊
				System.out.println(read);
			}
		} catch (IOException e) {
		    //
		}
	}
}