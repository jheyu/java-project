import java.io.*;
import java.net.*;
import java.lang.*;
import java.util.*;

public class Server {
	public static void main(String[] args) {
		int count = 0;
		ArrayList<DataOutputStream> clientOutputStreams = new ArrayList<DataOutputStream>();
		
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("請輸入port : ");
			int port = Integer.parseInt(input.readLine());
			System.out.println("Wating for connection...");

			ServerSocket serverSocket = new ServerSocket(port);

			while (true) {
				Socket clientSocket = serverSocket.accept();
				if (clientSocket.isConnected()) {
					try {
						count++;
						System.out.println("目前有" + count + "位Client");
						DataOutputStream dos = new DataOutputStream(clientSocket.getOutputStream());
						clientOutputStreams.add(dos);
						
						Thread thread = new Thread(new ClientAndTalk(clientSocket, clientOutputStreams));
						thread.start();
					} catch (Exception e) {
						count--;
						System.out.println(e.toString() + "count = " + count);
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}

	}
}

class ClientAndTalk implements Runnable {
	DataInputStream input;

	ArrayList<DataOutputStream> clientOutputStreams = new ArrayList<DataOutputStream>();
	protected int count;
	protected Socket clientSocket;

	public ClientAndTalk(Socket clientSocket, ArrayList<DataOutputStream> clientOutputStreams) {
		this.clientOutputStreams = clientOutputStreams;
		this.clientSocket = clientSocket;
		try {
			input = new DataInputStream(clientSocket.getInputStream());
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	public void run() 
	{
		try {
			while (true) {
				String read = input.readUTF();
				System.out.println(read);
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}
}