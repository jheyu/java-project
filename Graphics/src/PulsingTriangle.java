import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class PulsingTriangle {
    int x;
    int y;
    Color color;
    int[] xPoints = new int[3];
    int[] yPoints = new int[3];
    int xIncrement;
    int yIncrement;
    int xMax;
    int yMax;

    final int N_VERTICES = 3;
    final int SIZE = 30;
    public PulsingTriangle(int startX, int startY, Color startColor) {
        x = startX;
        y = startY;
        color = startColor;;
        setTriangle();
    }

    public void draw(Graphics surface) {
        drawSurface(surface);
        pulseTriangle();
        //detectLimit();
    }
    private void drawSurface(Graphics surface) {
        surface.setColor(color);
        surface.fillPolygon(xPoints, yPoints, N_VERTICES);
        surface.setColor(Color.GREEN);
        ((Graphics2D) surface).setStroke(new BasicStroke(3.0f));
        surface.drawPolygon(xPoints, yPoints, N_VERTICES);
    }
    private void setTriangle() {
        genXCoordinates();
        genYCoordinates();
    }
    private void genXCoordinates() {
        xPoints[0] = x;
        xPoints[1] = x + SIZE/2;
        xPoints[2] = x - SIZE/2;
    }
    private void genYCoordinates() {
        yPoints[0] = y;
        yPoints[1] = y + SIZE/2;
        yPoints[2] = y + SIZE/2;
    }
    public void pulseTriangle() {/*
        if(yPoints[0] >= y) {
        	yPoints[0] = yPoints[0] - SIZE*2;
        	yPoints[1] = yPoints[1] - SIZE*2;
        	yPoints[2] = yPoints[2] - SIZE*2;
        }else {
        	yPoints[0] = yPoints[0] + SIZE;
        	yPoints[1] = yPoints[1] + SIZE;
        	yPoints[2] = yPoints[2] + SIZE;
        }*/
    	double x0 = xPoints[0] - x;
    	double y0 = yPoints[0] - y;
    	
    	x0 = x0 * Math.toRadians(60) - y0 * Math.toRadians(60);
    	y0 = x0 * Math.toRadians(60) + y0 * Math.toRadians(60);

    	//TRANSLATE BACK
    	xPoints[0] = (int) (x0+x);
    	yPoints[0] = (int) (y0+y);
    	
    	double x1 = xPoints[1] - x;
    	double y1 = yPoints[1] - y;
    	
    	x1 = x1 * Math.toRadians(60) - y1 * Math.toRadians(60);
    	y1 = x1 * Math.toRadians(60) + y1 * Math.toRadians(60);

    	//TRANSLATE BACK
    	xPoints[1] = (int) (x1 + x);
    	yPoints[1] = (int) (y1 + y);
    	
    	double x2 = xPoints[2] - x;
    	double y2 = yPoints[2] - y;
    	
    	x2 = x2 * Math.toRadians(60) - y2 * Math.toRadians(60);
    	y2 = x2 * Math.toRadians(60) + y2 * Math.toRadians(60);

    	//TRANSLATE BACK
    	//xPoints[2] = (int) (x2 + x);
    	//yPoints[2] = (int) (y2 + y);
    }/*
    public void setPulseParams(double scaleFactor, double frequency) {
        xMax = (int)(SIZE * (scaleFactor - 1)/ 2.0 + 0.5);
        yMax = (int)(SIZE * (scaleFactor - 1) * 0.866 + 0.5);
        setIncrements(frequency);
    }
    private void setIncrements(double frequency) {
        xIncrement = frequencyToPixels(frequency, xMax);
        yIncrement = frequencyToPixels(frequency, yMax);
    }
    private int frequencyToPixels(double frequency, int axisMax) {
        return (int)(frequency * 50 * axisMax / 1000 + 0.5); // refresh rate = 50ms
    }
    private void detectLimit() {
       
    }*/
}