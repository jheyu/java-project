import java.io.IOException;

abstract class Human {
	public abstract void eat();
	public abstract void sleep();
	public abstract void beat();
}

class Man extends Human {
	public void eat() {
		System.out.println("Man can eat.");
	}
	
	public void sleep() {
		System.out.println("Man can sleep.");
	}
	
	public void beat() {
		System.out.println("Man can beat doudou.");
	}
}

class Female extends Human {
	public void eat() {
		System.out.println("Female can eat.");
	}
	
	public void sleep() {
		System.out.println("Female can sleep.");
	}
	
	public void beat() {
		System.out.println("Female can beat doudou.");
	}
}

public class Goddess {
    public static void main(String[] args) throws IOException {
    	Human human = HumanFactory.createHuman("female");
    	human.eat();
    	human.sleep();
    	human.beat();
    }
}
