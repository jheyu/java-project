
public class DecoratorMain {
	
	public static void main(String[] args) {
		Girl g = new Tall(new GoldenHair(new AmericanGirl()));
		System.out.println(g.getDescription());
	}

}
