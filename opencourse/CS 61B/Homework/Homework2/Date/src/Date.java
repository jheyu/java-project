/* Date.java */

import java.io.*;
import java.lang.*;
class Date {

  /* Put your private data fields here. */
  private int day_;
  private int month_;
  private int year_;
  private static int[] days = {31,28,31,30,31,30,31,31,30,31,30,31};
  
  /** Constructs a date with the given month, day and year.   If the date is
   *  not valid, the entire program will halt with an error message.
   *  @param month is a month, numbered in the range 1...12.
   *  @param day is between 1 and the number of days in the given month.
   *  @param year is the year in question, with no digits omitted.
   */
  public Date(int month, int day, int year) {
	  day_ = day;
	  month_ = month;
	  year_ = year;
	  
	  try {
		  if(isLeapYear(year_) && month_ == 2) {
			  if(day_ != 29)
				  throw new Exception();
		  } else if(month_ < 0 || month_ > 12) {
			  throw new Exception();
		  } else if(day_ < 0 || day_ > days[month-1]) {
			  throw new Exception();
		  } else if(year_ < 0) {
			  throw new Exception();
		  }
	  } catch(Exception e) {
		  System.out.println("Error message!!");
	  }
  }

  /** Constructs a Date object corresponding to the given string.
   *  @param s should be a string of the form "month/day/year" where month must
   *  be one or two digits, day must be one or two digits, and year must be
   *  between 1 and 4 digits.  If s does not match these requirements or is not
   *  a valid date, the program halts with an error message.
   */
  public Date(String s) {
	  String[] dates = s.split("/");

	  try {
		  if(dates[0].length() >= 0 && dates[0].length() <= 2)
			  month_ = Integer.parseInt(dates[0]);
		  else
			  throw new Exception();
		  
		  if(dates[1].length() >= 0 && dates[1].length() <= 2)
			  day_ = Integer.parseInt(dates[1]);
		  else
			  throw new Exception();
		  
		  if(dates[2].length() >= 0 && dates[2].length() <= 4)
			  year_ = Integer.parseInt(dates[2]);
		  else
			  throw new Exception();
	  } catch (Exception e) {
		  System.out.println("Error message!!");
	  }
      //System.out.println(month_+' '+day_);
  }

  /** Checks whether the given year is a leap year.
   *  @return true if and only if the input year is a leap year.
   */
  public static boolean isLeapYear(int year) {
	  // replace this line with your solution
	  if(year%400 == 0 || (year%4 == 0 && year%100 != 0))  
          return true;
	  else
		  return false;
  }

  /** Returns the number of days in a given month.
   *  @param month is a month, numbered in the range 1...12.
   *  @param year is the year in question, with no digits omitted.
   *  @return the number of days in the given month.
   */
  public static int daysInMonth(int month, int year) {
	  // replace this line with your solution
	  if(isLeapYear(year))
		  return 29;
	  return days[month-1];                           
  }

  /** Checks whether the given date is valid.
   *  @return true if and only if month/day/year constitute a valid date.
   *
   *  Years prior to A.D. 1 are NOT valid.
   */
  public static boolean isValidDate(int month, int day, int year) {
	  // replace this line with your solution
	  if(isLeapYear(year) && month == 2) {
		  if(day == 29)
			  return true;
	  } else if(day == days[month-1]) {
		  return true;
	  }
      return false;                        
  }

  /** Returns a string representation of this date in the form month/day/year.
   *  The month, day, and year are expressed in full as integers; for example,
   *  12/7/2006 or 3/21/407.
   *  @return a String representation of this date.
   */
  public String toString() {
	  // replace this line with your solution
	  String date = month_ + "/" + day_ + "/" + year_; 
      return date;                     
  }

  /** Determines whether this Date is before the Date d.
   *  @return true if and only if this Date is before d. 
   */
  public boolean isBefore(Date d) {
	  // replace this line with your solution
	  if(this.year_ < d.year_) {
		  return true;  
	  } else if(this.year_ == d.year_) {
		  if(this.month_ < d.month_) {
			  return true;
		  } else if(this.month_ == d.month_) {
			  if(this.day_ < d.day_)
				  return true;
			  else
				  return false;
		  } else {
			  return false;
		  }
	  } else {
         return false;
	  }
  }

  /** Determines whether this Date is after the Date d.
   *  @return true if and only if this Date is after d. 
   */
  public boolean isAfter(Date d) {
	  // replace this line with your solution
	  if(this.year_ > d.year_) {
		  return true;  
	  } else if(this.year_ == d.year_) {
		  if(this.month_ > d.month_) {
			  return true;
		  } else if(this.month_ == d.month_) {
			  if(this.day_ > d.day_)
				  return true;
			  else
				  return false;
		  } else {
			  return false;
		  }
	  } else {
         return false;
	  }                   
  }

  /** Returns the number of this Date in the year.
   *  @return a number n in the range 1...366, inclusive, such that this Date
   *  is the nth day of its year.  (366 is used only for December 31 in a leap
   *  year.)
   */
  public int dayInYear() {
	  // replace this line with your solution
	  int countDays = 0;
	  for(int i = 0; i < this.month_; i++) {
		  if(i == 1)
			  continue;
		  countDays += days[i];
	  }
	  
	  if(isLeapYear(year_))
		  countDays += 29;
	  else
		  countDays += 28;
      return countDays;                           
  }

  /** Determines the difference in days between d and this Date.  For example,
   *  if this Date is 12/15/2012 and d is 12/14/2012, the difference is 1.
   *  If this Date occurs before d, the result is negative.
   *  @return the difference in days between d and this date.
   */
  public int difference(Date d) {
	  // replace this line with your solution
	  int countDays = 0;
	  // 如果年份一樣
	  if(this.year_ == d.year_) {
		  if(this.month_ > d.month_) {
			  for(int i = d.month_ + 1; i < this.month_; i++) {
				  if(i == 2) {
					  if(isLeapYear(this.year_))
						  countDays += 29;
					  else
						  countDays += 28;
				  }
				  countDays += days[i-1];
		      }
			  countDays += days[d.month_] - d.day_ + this.day_;
		  } else if(this.month_ < d.month_){
			  for(int i = this.month_ + 1; i < d.month_; i++) {
				  if(i == 2) {
					  if(isLeapYear(this.year_))
						  countDays += 29;
					  else
						  countDays += 28;
				  }
				  countDays += days[i-1];
		      }
			  countDays += days[this.month_] - this.day_ + d.day_;
		  } else {
			  if(this.day_ > d.day_)
				  countDays += this.day_ - d.day_;
			  else
				  countDays += d.day_ - this.day_;
		  }
		  return countDays;
	  } else { // 如果年份不一樣
		  int minYear, maxYear;
		  int minMonth, maxMonth;
		  int minDay, maxDay;
		  if(this.year_ > d.year_)  {
			  maxYear = this.year_;
			  maxMonth = this.month_;
			  maxDay = this.day_;
			  minYear = d.year_;
			  minMonth = d.month_;
			  minDay = d.day_;
		  }
		  else {
			  maxYear = d.year_;
			  maxMonth = d.month_;
			  maxDay = d.day_;
			  minYear = this.year_;
			  minMonth = this.month_;
			  minDay = this.day_;
		  }
		   
			// 先加入兩者中間幾年的年份天數  
		    for(int i = minYear+1; i < maxYear; i++) {
			  if(isLeapYear(i))
				  countDays += 366;
			  else
				  countDays += 365;
		    }
		    System.out.println(countDays);
		    // 加入較小年份的當年天數,要從當天的下個月開始加到年尾
		    for(int i = minMonth+1; i<=12; i++) {
			  if(i == 2) {
				  if(isLeapYear(d.year_))
					  countDays += 29;
				  else
					  countDays += 28;
				  continue;
			  }
			  countDays += days[i-1];
		    }
		    System.out.println(countDays);
		    // 加入較小年份的當月天數
		    if(minMonth == 2 && isLeapYear(minYear))
			   countDays += 29 - minDay;
		    else
		    	countDays += days[minMonth - 1] - minDay;
		    System.out.println(countDays);
		    // 加入較大年份的當年天數,要從當年年頭加到當天
		    for(int i = 1; i<maxMonth; i++) {
				  if(i == 2) {
					  if(isLeapYear(maxYear))
						  countDays += 29;
					  else
						  countDays += 28;
					  continue;
				  }
				  countDays += days[i-1];
			}
		    System.out.println(countDays);
		    countDays += maxDay;
		    if(this.year_ > d.year_)
		    	return countDays;
		    else
		    	return 0-countDays;
	  }
	  
  }

  public static void main(String[] argv) {
	  
    System.out.println("\nTesting constructors.");
    Date d1 = new Date(1, 1, 1);
    System.out.println("Date should be 1/1/1: " + d1);
    d1 = new Date("2/4/2");
    System.out.println("Date should be 2/4/2: " + d1);
    d1 = new Date("2/29/2000");
    System.out.println("Date should be 2/29/2000: " + d1);
    d1 = new Date("2/29/1904");
    System.out.println("Date should be 2/29/1904: " + d1);

    d1 = new Date(12, 31, 1975);
    System.out.println("Date should be 12/31/1975: " + d1);
    Date d2 = new Date("1/1/1976");
    System.out.println("Date should be 1/1/1976: " + d2);
    Date d3 = new Date("1/2/1976");
    System.out.println("Date should be 1/2/1976: " + d3);

    Date d4 = new Date("2/27/1977");
    Date d5 = new Date("8/31/2110");

    /* I recommend you write code to test the isLeapYear function! */
    System.out.println("\nThat 2014 is leap year should be " + isLeapYear(2014));
    System.out.println("That 1900 is leap year should be " + isLeapYear(1900));
    System.out.println("That 1600 is leap year should be " + isLeapYear(1600));
    System.out.println("That 2012 is leap year should be " + isLeapYear(2012));
    
    System.out.println("\nTesting before and after.");
    System.out.println(d2 + " after " + d1 + " should be true: " + d2.isAfter(d1));
    System.out.println(d3 + " after " + d2 + " should be true: " + d3.isAfter(d2));
    System.out.println(d1 + " after " + d1 + " should be false: " + d1.isAfter(d1));
    System.out.println(d1 + " after " + d2 + " should be false: " + d1.isAfter(d2));
    System.out.println(d2 + " after " + d3 + " should be false: " + d2.isAfter(d3));
    System.out.println(d1 + " before " + d2 + " should be true: " + d1.isBefore(d2));
    System.out.println(d2 + " before " + d3 + " should be true: " + d2.isBefore(d3));
    System.out.println(d1 + " before " + d1 + " should be false: " + d1.isBefore(d1));
    System.out.println(d2 + " before " + d1 + " should be false: " + d2.isBefore(d1));
    System.out.println(d3 + " before " + d2 + " should be false: " + d3.isBefore(d2));

    System.out.println("\nTesting difference.");
    System.out.println(d1 + " - " + d1  + " should be 0: " + d1.difference(d1));
    System.out.println(d2 + " - " + d1  + " should be 1: " + d2.difference(d1));
    System.out.println(d3 + " - " + d1  + " should be 2: " + d3.difference(d1));
    System.out.println(d3 + " - " + d4  + " should be -422: " + d3.difference(d4));
    System.out.println(d5 + " - " + d4  + " should be 48762: " + d5.difference(d4));
  }
  
}
