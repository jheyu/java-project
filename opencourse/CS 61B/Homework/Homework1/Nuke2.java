/* Nuke2.java */

import java.io.*;

/**  a class called Nuke2 whose main method reads a string from the 
 *   keyboard and prints the same string, with its second character removed.
 */

class Nuke2 {

  public static void main(String[] arg) throws Exception {

    BufferedReader keyboard;
    String inputLine;

    keyboard = new BufferedReader(new InputStreamReader(System.in));

    System.out.print("Please enter the name of a company (without spaces): ");
    System.out.flush();        /* Make sure the line is printed immediately. */
    inputLine = keyboard.readLine();

    /* Replace this comment with your solution.  */

  }
}