
public class BadTransactionException extends Exception {
	
	public int amount; 

	  /**
	   *  Creates an exception object for nonexistent account "badAcctNumber".
	   **/
	  public BadTransactionException(int badAmount) {
	    super("Invalid amount: " + badAmount);
	    amount = badAmount;
	  }

}
