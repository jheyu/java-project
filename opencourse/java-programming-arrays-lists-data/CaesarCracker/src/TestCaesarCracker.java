
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TestCaesarCracker {
	
	public static void testCaesarCracker1() {
		String input = "", line;
		try {
	        FileReader file = new FileReader("titus-small_key5.txt");
	        BufferedReader br = new BufferedReader(file);
	        while((line = br.readLine()) != null) {
	            input += line;	
		    }
		}
		catch(IOException e) {
			System.out.println(e);
		}	
		
		CaesarCracker cc = new CaesarCracker();
		System.out.println("CaesarCracker1: " + cc.decrypt(input) + "\n");
	}
	
	public static void testCaesarCracker2() {
		String input = "", line;
		try {
	        FileReader file = new FileReader("oslusiadas_key17.txt");
	        BufferedReader br = new BufferedReader(file);
	        while((line = br.readLine()) != null) {
	            input += line;	
		    }
		}
		catch(IOException e) {
			System.out.println(e);
		}	
		
		CaesarCracker cc = new CaesarCracker('a');
		System.out.println("CaesarCracker2: " + cc.decrypt(input) + "\n");
	}
	
	public static void main(String[] args) {
		testCaesarCracker2();
	}

}
