
public class CaesarCracker {
	char mostCommon;
	
	public CaesarCracker() {
		mostCommon = 'e';
	}
	
	public CaesarCracker(char c) {
		mostCommon = c;
	}
	
	public int[] countLetters(String encrypted) {
        String alph = "abcdefghijklmnopqrstuvwxyz";
        int[] counters = new int[26];
        for(int i = 0; i < encrypted.length(); ++i) {
        	int idx = alph.indexOf(Character.toLowerCase(encrypted.charAt(i)));
        	if(idx != -1) {
        		counters[idx]++;
        	}
        }
        return counters;
	}
	
	public int maxIndex(int[] freqs) {
		int maxIdx = 0;
		for(int i = 0; i < freqs.length; ++i) {
			if(freqs[maxIdx] < freqs[i]) {
				maxIdx = i;
			}
		}
		return maxIdx;
	}
	
	public int getKey(String encrypted) {
	    int[] freqs = countLetters(encrypted);
	    int maxIdx = maxIndex(freqs);
	    int mostCommonPos = mostCommon - 'a';
	    int dkey = maxIdx - mostCommonPos;
	    if(maxIdx < mostCommonPos) {
	    	dkey = 26 - (mostCommonPos - maxIdx);
	    }
	    return dkey;
	}
	
	public String decrypt(String encrypted) {
		int key = getKey(encrypted);
		CaesarCipher cc = new CaesarCipher(key);
		return cc.decrypt(encrypted);
	}

}
