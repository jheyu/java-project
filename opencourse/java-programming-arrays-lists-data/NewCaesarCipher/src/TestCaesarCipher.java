
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TestCaesarCipher {
	
	public static void testCaesarCipher() {
		String input = "", line;
		try {
	        FileReader file = new FileReader("titus-small.txt");
	        BufferedReader br = new BufferedReader(file);
	        while((line = br.readLine()) != null) {
	            input += line;	
		    }
		}
		catch(IOException e) {
			System.out.println(e);
		}
		
		 CaesarCipher cc = new CaesarCipher(5);
		 String encryString = cc.encrypt(input);
	     System.out.println("CaesarCipher: " + encryString + "\n");
	     System.out.println(cc.decrypt(encryString));
	}
	
	public static void main(String[] args) {
		testCaesarCipher();
	}

}
