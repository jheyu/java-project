
public class CaesarCipher {
	
    public static String encrypt(String input, int key) {
        StringBuilder encrypted = new StringBuilder(input);
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String shiftedAlphabet = alphabet.substring(key, 26) + alphabet.substring(0, key);
        String newstring = new String();
        for(int i = 0; i < encrypted.length(); i++) {
            char currChar = encrypted.charAt(i);
            //currChar = Character.toUpperCase(currChar);
            int idx = alphabet.indexOf(currChar);
            if(idx == -1){
            	idx = alphabet.toLowerCase().indexOf(input.substring(i, i+1));
                if(idx > -1)
                	newstring = newstring + shiftedAlphabet.substring(idx, idx+1).toLowerCase();
                else
                	newstring = newstring + input.substring(i, i+1);
                	//encrypted.setCharAt(i, newChar);
            }else {
            	newstring = newstring + shiftedAlphabet.substring(idx, idx+1);
                //encrypted.setCharAt(i, newChar);
            }
        }
        return newstring;
    }
    
    public void testencrypt() {
    	String encrypted = encrypt("First Legion", 17);
    	System.out.println("key is " + 23 + "\n" + encrypted);
    }
    
    public String encryptTwokeys(String input, int key1, int key2) {
        String newstring = new String();
        for(int i = 0; i < input.length(); i++) {
            char currChar = input.charAt(i);
            if(i%2 == 0)
            	newstring = newstring + encrypt(input, key1).substring(i, i+1);
            else
            	newstring = newstring + encrypt(input, key2).substring(i, i+1);
        }
        return newstring;
    }
    
    public void testencryptTwoKeys() {
    	String encrypted = encryptTwokeys("First Legion", 23, 17);
    	System.out.println("key is " + 23 + " and " + 17 + "\n" + encrypted);
    }
    
    public static void main(String[] args) {
    	CaesarCipher test = new CaesarCipher();
    	test.testencrypt();
    	test.testencryptTwoKeys();
    }
}

