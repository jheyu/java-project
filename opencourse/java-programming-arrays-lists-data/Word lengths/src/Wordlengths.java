import java.io.*;

public class Wordlengths {
	
	public void countWordLengths(FileReader resource, int[] count) {
		BufferedReader br = new BufferedReader(resource);
		String word;
		
		try{
	        while((word = br.readLine()) != null){
	        	String[] token = word.split("[ |,|.]");
	        	for(int i = 0; i < token.length; i++) {
	        		if(token[i].length() <= 0 && token[i].trim().isEmpty())
	        			continue;
	        	    int length = token[i].length();
	        	    count[length-1] += 1;
	        	}
		    }
		}
		catch(IOException e){
			System.out.println(e);
		}
	}
	
	public int indexOfMax(int[] values) {
		int maxIndex = 0;
		for(int i = 0; i < values.length; i++) {
			if(values[i] > 0)
				maxIndex = i;
		}
		
		return maxIndex;
	}
	
	public static void main(String[] args) {
		Wordlengths obj = new Wordlengths();
		int[] count = new int[31];
		try {
		    FileReader file = new FileReader("smallHamlet.txt");
		    obj.countWordLengths(file, count);
		}
		catch(IOException e) {
			System.out.println(e);
		}
		int maxlength = obj.indexOfMax(count);
		System.out.println("The most common word length in the file is " + (maxlength+1));
	}

}
