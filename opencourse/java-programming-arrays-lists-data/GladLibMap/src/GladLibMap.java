import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class GladLibMap {
	private Map<String, ArrayList<String>> labelToWordsMap;
	
	private Random myRandom;
	
	private static String dataSourceURL = "http://dukelearntoprogram.com/course3/data";
	private static String dataSourceDirectory = "data";
	
	public GladLibMap(){
		initializeFromSource(dataSourceDirectory);
		myRandom = new Random();
	}
	
	public GladLibMap(String source){
		initializeFromSource(source);
		myRandom = new Random();
	}
	
	private void initializeFromSource(String source) {
		labelToWordsMap = new HashMap<>();
		String[] labels = {"country", "noun", "animal",
				   "adjective", "name", "color",
				   "timeframe", "verb", "fruit"};
		
		for(String label :labels) {
			ArrayList<String> list = readIt(source + "/" + label + ".txt");
		    labelToWordsMap.put(label, list);
		}
		
		myRandom = new Random();
	}
	
	private String randomFrom(ArrayList<String> source){
		int index = myRandom.nextInt(source.size());
		return source.get(index);
	}
	
	private String getSubstitute(String label) {
		if (label.equals("number")){
			return ""+myRandom.nextInt(50)+5;
		}
		return randomFrom(labelToWordsMap.get(label));
	}
	
	private String processWord(String w){
		int first = w.indexOf("<");
		int last = w.indexOf(">",first);
		if (first == -1 || last == -1){
			return w;
		}
		String prefix = w.substring(0,first);
		String suffix = w.substring(last+1);
		String sub = getSubstitute(w.substring(first+1,last));
		return prefix+sub+suffix;
	}
	
	private void printOut(String s, int lineWidth){
		int charsWritten = 0;
		for(String w : s.split("\\s+")){
			if (charsWritten + w.length() > lineWidth){
				System.out.println();
				charsWritten = 0;
			}
			System.out.print(w+" ");
			charsWritten += w.length() + 1;
		}
	}
	
	private String fromTemplate(String source){	
		String line = "", story = "";
		/*
		if (source.startsWith("http")) {
			URLResource resource = new URLResource(source);
			for(String word : resource.words()){
				story = story + processWord(word) + " ";
			}
		}
		else {*/
			try{
				FileReader file = new FileReader(source);
			    BufferedReader br = new BufferedReader(file);
		        while((line = br.readLine()) != null){
		        	String[] token = line.split("\\s+");
		        	for(int i = 0; i < token.length; i++) {
		        	    story = story + processWord(token[i]) + " ";
		        	}
			    }
			}
			catch(IOException e){
				System.out.println(e);
			}
		//}
		return story;
	}
	
	private ArrayList<String> readIt(String source){
		ArrayList<String> list = new ArrayList<String>();
		String line = "";
		/*
		if (source.startsWith("http")) {
			URLResource resource = new URLResource(source);
			for(String line : resource.lines()){
				list.add(line);
			}
		}
		else {*/
		try{
			FileReader file = new FileReader(source);
		    BufferedReader br = new BufferedReader(file);
	        while((line = br.readLine()) != null){
	        	list.add(line);
		    }
		}
		catch(IOException e){
			System.out.println(e);
		}
		//}
		return list;
	}
	
	public void makeStory(){
	    System.out.println("\n");
		String story = fromTemplate("data/madtemplate2.txt");
		printOut(story, 60);
	}
	
    public static void main(String[] args) {
    	GladLibMap glb = new GladLibMap();
    	glb.makeStory();
    }

}
