
public class wordplay {
	
	public boolean isVowel(char ch) {
		ch = Character.toLowerCase(ch);
		if (ch == 'a' || ch == 'e' || ch == 'i' ||
				ch == 'o' || ch == 'u') {
			return true;
		}
		return false;
	}
	
	public void TestisVowel() {
		boolean testF = isVowel('F');
		System.out.println("Test char 'F': " + testF );
		boolean testA = isVowel('A');
		System.out.println("Test char 'A': " + testA);
	}
	
	public StringBuilder replaceVowels(StringBuilder phrase, char ch) {
		for(int i = 0 ; i < phrase.length(); i++) {
			char word = Character.toLowerCase(phrase.charAt(i));
			if(isVowel(word)) {
				phrase.setCharAt(i, ch);
			}
		}
		return phrase;
	}
	
	public void TestreplaceVowels() {
		StringBuilder testStr = new StringBuilder("Hello World");
		System.out.println("Test string is 'Hello World': " + replaceVowels(testStr, '*'));
	}
	
	public StringBuilder emphasize(StringBuilder phrase, char ch) {
		for(int i = 0 ; i < phrase.length(); i++) {
			char word = Character.toLowerCase(phrase.charAt(i));
			if(word == ch && i%2 == 0) {
				phrase.setCharAt(i, '*');
			} 
			else if(word == ch && i%2 != 0) {
				phrase.setCharAt(i, '+');
			} 
		}
		return phrase;
	}
	
	public void Testemphasize() {
		StringBuilder testStr1 = new StringBuilder("dna ctgaaactga");
		System.out.println("Test string is 'dna ctgaaactga': " + emphasize(testStr1, 'a'));
		
		StringBuilder testStr2 = new StringBuilder("Mary Bella Abracadabra");
		System.out.println("Test string is 'Mary Bella Abracadabra': " + emphasize(testStr2, 'a'));
	}
	
	public static void main(String[] args) {
		wordplay wp = new wordplay();
	    wp.TestisVowel();
	    wp.TestreplaceVowels();
	    wp.Testemphasize();
	}

}
