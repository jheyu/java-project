import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TestVigenereCipher {
	
	public static void testVigenereCipher() {
		String input = "", line;
		try {
	        FileReader file = new FileReader("titus-small.txt");
	        BufferedReader br = new BufferedReader(file);
	        while((line = br.readLine()) != null) {
	            input += line;	
		    }
		}
		catch(IOException e) {
			System.out.println(e);
		}	
		
		VigenereCipher vc = new VigenereCipher(new int[]{17, 14, 12, 4});
        System.out.println("VigenereCipher encrypt: " + vc.encrypt(input) + "\n");
        System.out.println("VigenereCipher decrypt: " + vc.decrypt(vc.encrypt(input)) + "\n");
	}
	
	public static void main(String[] args) {
		testVigenereCipher();
	}


}
