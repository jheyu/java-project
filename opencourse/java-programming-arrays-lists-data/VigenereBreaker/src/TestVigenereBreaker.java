import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TestVigenereBreaker {
	
	public static void main(String[] args) {
		VigenereBreaker vb = new VigenereBreaker();
		String input = "", line;
		try {
	        FileReader file = new FileReader("athens_keyflute.txt");
	        BufferedReader br = new BufferedReader(file);
	        while((line = br.readLine()) != null) {
	            input += line;	
		    }
		}
		catch(IOException e) {
			System.out.println(e);
		}	
		//System.out.println(input);
		int[] key = vb.tryKeyLength(input, 5, 'e');
		for(int i = 0; i < key.length; ++i) {
			System.out.println(key[i]);
		}
		VigenereCipher vc = new VigenereCipher(key);
		String result = vc.decrypt(input);
	    System.out.println(result);
	}

}
