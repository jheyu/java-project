
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class WordsinFiles {
	
	private Map<String, List<String>> wordFileMap;
	
	public WordsinFiles() {
		wordFileMap = new HashMap<>();
	}
	
	private void addWordsFromFile(String f) {
	    String line = "";
		try{
		    FileReader file = new FileReader(f);
		    BufferedReader br = new BufferedReader(file);
		    while((line = br.readLine()) != null){
	        	String[] token = line.split("\\s+");
	        	for(int i = 0; i < token.length; i++) {
	        		List<String> ff = wordFileMap.get(token[i]);
	        	    if(wordFileMap.containsKey(token[i]) && !ff.contains(f)) {
	        	    	ff.add(f);
	        	    }
	        	    if(!wordFileMap.containsKey(token[i])) {	
	        	    	List<String> ff2 = new ArrayList<>();
	        	    	ff2.add(f);
	        	    	wordFileMap.put(token[i], ff2);
	        	    }
	        	}
		    }
		}
		catch(IOException e){
			System.out.println(e);
		}
		
	}
	
	public void buildWordFileMap() {
        wordFileMap.clear();
        for(int i = 1; i <= 4; i++)
            addWordsFromFile("brief"+i+".txt");
	}
	
	public int maxNumber() {
		int max = 0;
		for(Entry<String, List<String>> entry : wordFileMap.entrySet()) {
			if(entry.getValue().size() > max)
				max = entry.getValue().size();
		}
		return max;
	}
	
	public List<String> wordsInNumFiles(int number) {
		List<String> wordList = new ArrayList<String>();
		for(Entry<String, List<String>> entry : wordFileMap.entrySet()) {
			if(entry.getValue().size() == number)
				wordList.add(entry.getKey());
		}
		return wordList;
	}
	
	public void printFilesIn(String word) {
		for(Entry<String, List<String>> entry : wordFileMap.entrySet()) {
			if(entry.getKey() == word) {
				for(int i = 0; i < entry.getValue().size(); i++)
					System.out.print(entry.getValue().indexOf(i) + " ");
			}
		}
	}
	
	public void tester() {
		buildWordFileMap();
		int number = maxNumber();
		List<String> words = wordsInNumFiles(number);
		
		System.out.println("Max num of files: " + number);
		System.out.println("Total of " + words.size() + " words that are in " + number + " files:");
		for (String word : words) {
			System.out.println("\"" + word + "\" appears in the files:");
			for (String file : wordFileMap.get(word)) {
				System.out.println("\t" + file);
			}
		}
	}

	public static void main(String[] args) {
		WordsinFiles wif = new WordsinFiles();
		wif.tester();
	}
	
}
