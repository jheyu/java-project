
/**
 * Write a description of class LogAnalyzer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class LogAnalyzer
{
     private ArrayList<LogEntry> records;
     
     public LogAnalyzer() {
         // complete constructor
    	 records = new ArrayList<>();
     }
        
     public void readFile(String filename) {
         // complete method
    	 String line = "";
 		 try{
 		     FileReader file = new FileReader(filename);
 		     BufferedReader br = new BufferedReader(file);
 		     while((line = br.readLine()) != null){
 	        	 //String[] token = line.split("\\s+");
 	        	 //for(int i = 0; i < token.length; i++) {
 	        	     //System.out.println(line);
 	        		 records.add(WebLogParser.parseEntry(line));
 	        	 //}
 		     }
 		 }
 		 catch(IOException e){
 			 System.out.println(e);
 		 }
     }
     
     public int countUniqueIPs() {
         ArrayList<String> uniqueIPs = new ArrayList<String>();
         for(LogEntry le: records) {
        	 String ipAddr = le.getIpAddress();
        	 if(!uniqueIPs.contains(ipAddr)) {
        		 uniqueIPs.add(ipAddr);
        	 }
         }
         return uniqueIPs.size();
     }
     
     public List<String> uniqueIPVisitsOnDay(String someday) {
    	 List<String> uniqueVisits = new ArrayList<>();
    	 for (LogEntry le : records) {
    		 Date acessTime = le.getAccessTime();
    		 String ipAddr = le.getIpAddress();
    		 String dateToString = acessTime.toString();
    		 String[] token = dateToString.split("\\s+");
    		 String date = token[1] + " " + token[2];
    		 if(date.equals(someday) && !uniqueVisits.contains(ipAddr)) {
    			 uniqueVisits.add(ipAddr);		 
    		 } 	
    	 }
    	 
    	 return uniqueVisits;
     }
     
     public void printAllHigherThanNum(int num) {
    	 System.out.println("All log entries with status higher than " + num + ":");
    	 for (LogEntry le : records) {
    		 if(le.getStatusCode() > num)
    			 System.out.println(le);
    	 }	 
     }
     
     public int countUniqueIPsInRange(int low, int high) {
    	 List<String> uniqueIPs = new ArrayList<>();
    	 for (LogEntry le : records) {
    	     String ipAddr = le.getIpAddress();
    	     if(low <= le.getStatusCode() && le.getStatusCode() <= high && !uniqueIPs.contains(ipAddr)) {
    	    	 uniqueIPs.add(ipAddr);
    	     }
    	 }
    	 return uniqueIPs.size();
     }
     
     public HashMap<String, Integer> countVisitsPerIP() {
    	 HashMap<String, Integer> counts = new HashMap<String, Integer>();
    	 for(LogEntry le : records) {
    		 String ipAddr = le.getIpAddress();
    		 if(!counts.containsKey(ipAddr)) {
    			 counts.put(ipAddr, 1);
    		 }else {
    			 counts.put(ipAddr, counts.get(ipAddr)+1);
    		 }
    	 }
    	 return counts;
     }
     
     public int mostNumberVisitsByIP(Map<String, Integer> visitsPerIPMap) {
    	 int result = 0;
    	 for(Entry<String, Integer> entry: visitsPerIPMap.entrySet()) {
    		 if(entry.getValue() > result)
    			 result = entry.getValue();
    	 }
    	 return result;
     }
     
     public List<String> iPsMostVisits(Map<String, Integer> visitsPerIPMap) {
    	 List<String> result = new ArrayList<String>();
    	 int most = 0;
    	 
    	 LogAnalyzer analyzer = new LogAnalyzer();
    	 most = analyzer.mostNumberVisitsByIP(visitsPerIPMap);
    	 for(Entry<String, Integer> entry: visitsPerIPMap.entrySet()) {
    		 if(entry.getValue() == most)
    			 result.add(entry.getKey());
    	 }
    	 
    	 return result;
     }
     
     public Map<String, List<String>> iPsForDays() {
    	 Map<String, List<String>> result = new HashMap<String, List<String>>();
    	 
    	 for (LogEntry le : records) {
    		 Date acessTime = le.getAccessTime();
    		 String ipAddr = le.getIpAddress();
    		 String dateToString = acessTime.toString();
    		 String[] token = dateToString.split("\\s+");
    		 String date = token[1] + " " + token[2];
    		 
    		 if(!result.containsKey(date)) {
    			 List<String> ips = new ArrayList<>();
        		 ips.add(ipAddr);
    			 result.put(date, ips);
    		 }else {
    			 result.get(date).add(ipAddr);
    		 }
    	 }
    	 return result;
     }
     
     public String dayWithMostIPVisits(Map<String, List<String>> ipsForDays) {
    	 int most = 0;
    	 String day = "";
    	 for(Entry<String, List<String>> entry: ipsForDays.entrySet()) {
    		 if(entry.getValue().size() > most)
    			 day = entry.getKey();
    	 }
    	 return day;
     }
     
     public List<String> iPsWithMostVisitsOnDay(Map<String, List<String>> ipsForDays, String someday) {
    	 List<String> result = new ArrayList<String>();
    	 
    	 LogAnalyzer analyzer = new LogAnalyzer();
    	 HashMap<String, Integer> counts = new HashMap<String, Integer>();
    	 List<String> ips = ipsForDays.get(someday);
    	 for(int i = 0; i < ips.size(); ++i) {
    		 String ip = ips.get(i);
    		 if(!counts.containsKey(ip)) {
    			 counts.put(ip, 1);
    		 }else {
    			 counts.put(ip, counts.get(ip)+1);
    		 }
    	 }
    	 
    	 int most = 0;
    	 most = analyzer.mostNumberVisitsByIP(counts);
    	 for(Entry<String, Integer> entry: counts.entrySet()) {
    		 if(entry.getValue() == most)
    			 result.add(entry.getKey());
    	 }
    	 return result;
     }
     
     public void printAll() {
         for (LogEntry le : records) {
             System.out.println(le);
         }
     }
}
